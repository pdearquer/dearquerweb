
# deArquer
Página de inicio de deArquer.com (previamente Querar.com) convertida en extensión del navegador.

## Instrucciones de Uso
Al abrir la página aparece una barra de entrada de texto y una lista de enlaces con iconos.
* Pulsa en un enlace para ir a ese sitio.
* Cuando escribes en la barra de texto, los enlaces se actualizan para ir una búsqueda en el sitio. No todos los sitios soportan búsqueda.
* Las teclas F1-F12 sirven de acceso rápido. Por ejemplo pulsa F2 para ir al segundo enlace.
* La tecla Intro te lleva al primer enlace. De esta forma puedes escribir un texto y pulsar Intro para usar tu buscador favorito como lo harías habitualmente.
* La barra de búsqueda muestra sugerencias, pero al pulsar en una no se lanza la búsqueda directamente. Esto te permite elegir una sugerencia similar a lo que buscas y editarla para poner exactamente lo que quieres.
* Si abres un enlace pulsando además la tecla Control, el sitio se abre en una pestaña nueva.

## Personalización
Si quieres personalizar los enlaces, descarga el respositorio en una carpeta conveniente (estable), haz una copia del fichero `res/default.js` como `res/config.js` y edítalo a tu gusto. Tienes un ejemplo más completo en `res/config_example.js`. Las categorías aún no están implementadas, es decir que no se usan.

Si necesitas añadir un icono nuevo simplemento pon la imagen en la carpeta `img`. Las imágenes tienen que estar en formato PNG y de tamaño 100x100 pixels. El nombre de la imagen (sin extensión) es el que se usa en `res/config.js`.

## Instalación en Chrome
Ve al menú `Mas Herramientas...`, `Extensiones`. Activa el `Modo de desarrollador` en el botón de arriba a la derecha. Pulsa `Cargar descomprimida` y selecciona la carpeta dónde descargaste este proyecto.

## Instalación en Firefox
Firefox pone muchas restricciones a extensiones no firmadas, lo que hace la instalación más complicada.

Primero ve a la URL `about:config`, confirma que quieres entrar, busca la variable `xpinstall.signatures.required` y asígnale el valor `false`.

Después comprime el contenido de este proyecto en un ZIP, abre la URL `about:addons`, pulsa el botón que tiene un icono de engranaje y selecciona `Instalar complemento desde archivo...`. Selecciona el comprimido que creaste anteriormente y acepta todas las advertencias. Se recomienda activar su uso en modo privado ya que no presenta ningún peligro para la privacidad. Además tienes que abrir una pestaña nueva y confirmar que quieres usar la extensión pulsando en `Conservar cambios` y después pulsar el botón de inicio y confirmar de nuevo. Si no haces este último paso parece que Firefox elimina la extensión al reiniciar.

También puedes hacer una instalación temporal para probar la extensión (o cambios que hagan en ella), pero al reiniciar el navegador la extensión no se mantiene. Para ello ve a la URL `about:debugging` y pulsa en la sección del menú lateral llamada `Este Firefox`. Después pulsa el botón `Cargar complemento temporal...` y selecciona el fichero `manifest.json` de este proyecto.

## Instalación en Otros Navegadores
Otra opción de instalación es simplemente definir como página de inicio el fichero `start.html`. No tiene buen soporte en general, en especial al abrir pestañas nuevas, pero no requiere ninguna instalación adicional.

