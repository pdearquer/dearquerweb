var config = [
  {
    "name": "Búsqueda",
    "links": [
      "google",
      "gimages",
      "gmaps",
      "wikies",
      "wikien",
      "rae",
      "wordref"
    ]
  },
  {
    "name": "Trabajo",
    "links": [
      "gmail",
      "github",
      {
         "image": "gitlab",
         "description": "Gitlab",
         "url": "https://gitlab.com/colnix"
      },
      "dearquer",
      "colnix"
    ]
  },
  {
    "name": "Social",
    "links": [
      "whatsapp",
      "linkedin",
      "youtube",
      "facebook",
      "twitter"
    ]
  }
];
