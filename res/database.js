var database = {
  "adwords": {
    "image": "adwords",
    "description": "Adwords",
    "url": "https://ads.google.com/aw/overview"
  },
  "aliexpress": {
    "image": "aliexpress",
    "description": "Aliexpress",
    "url": "https://es.aliexpress.com",
    "search": "https://es.aliexpress.com/w/wholesale-${query}.html"
  },
  "amazon": {
    "image": "amazon",
    "description": "Amazon",
    "url": "https://www.amazon.es/",
    "search": "https://www.amazon.es/s?k=${query}"
  },
  "amazonseller": {
    "image": "amazonseller",
    "description": "Amazon Seller",
    "url": "https://sellercentral.amazon.es/home"
  },
  "bbva": {
    "image": "bbva",
    "description": "BBVA",
    "url": "https://www.bbva.es/"
  },
  "chatgpt": {
    "image": "chatgpt",
    "description": "ChatGPT",
    "url": "https://chatgpt.com/"
  },
  "clickup": {
    "image": "clickup",
    "description": "ClickUp",
    "url": "https://app.clickup.com/"
  },
  "colnix": {
    "image": "colnix",
    "description": "Colnix Web",
    "url": "https://www.colnix.com/"
  },
  "coursera": {
    "image": "coursera",
    "description": "Coursera",
    "url": "https://www.coursera.org",
    "search": "https://www.coursera.org/courses?query=${query}"
  },
  "dearquer": {
    "image": "dearquer",
    "description": "deArquer",
    "url": "https://www.dearquer.com/"
  },
  "digikey": {
    "groups": [
      "shop"
    ],
    "image": "digikey",
    "description": "Digikey",
    "url": "https://www.digikey.es/",
    "search": "https://www.digikey.es/products/es?keywords=${query}"
  },
  "duckduckgo": {
    "groups": [
      "search"
    ],
    "image": "duckduckgo",
    "description": "DuckDuckGo",
    "url": "https://duckduckgo.com/",
    "search": "https://duckduckgo.com?q=${query}"
  },
  "facebook": {
    "groups": [
      "social"
    ],
    "image": "facebook",
    "description": "Facebook",
    "url": "https://www.facebook.com/"
  },
  "farnell": {
    "groups": [
      "shop"
    ],
    "image": "farnell",
    "description": "Farnell",
    "url": "http://es.farnell.com/",
    "search": "http://es.farnell.com/search?st=${query}"
  },
  "freedict": {
    "groups": [
      "dictionary"
    ],
    "image": "freedict",
    "description": "The Free Dictionary",
    "url": "http://www.thefreedictionary.com/",
    "search": "http://www.thefreedictionary.com/${query}"
  },
  "ganalytics": {
    "image": "ganalytics",
    "description": "Google Analytics",
    "url": "https://analytics.google.com/"
  },
  "gcalendar": {
    "image": "gcalendar",
    "description": "Google Calendar",
    "url": "https://calendar.google.com/"
  },
  "gcloud": {
    "image": "gcloud",
    "description": "Google Cloud",
    "url": "https://console.cloud.google.com/"
  },
  "gdrive": {
    "image": "gdrive",
    "description": "Google Drive",
    "url": "https://drive.google.com/drive/",
    "search": "https://drive.google.com/drive/search?q=${query}"
  },
  "gimages": {
    "groups": [
      "search"
    ],
    "image": "gimages",
    "description": "Google Images",
    "url": "https://www.google.com/imghp",
    "search": "https://www.google.com/search?tbm=isch&source=hp&q=${query}"
  },
  "gitlab": {
    "groups": [
      "development"
    ],
    "image": "gitlab",
    "description": "Gitlab",
    "url": "https://gitlab.com",
    "search": "https://gitlab.com/search?search=${query}"
  },
  "github": {
    "groups": [
      "development"
    ],
    "image": "github",
    "description": "Github",
    "url": "https://github.com/",
    "search": "https://github.com/search?q=${query}"
  },
  "gkeywords": {
    "image": "gkeywords",
    "description": "Keyword Planner",
    "url": "https://ads.google.com/aw/keywordplanner/home"
  },
  "google": {
    "image": "google",
    "description": "Google Web",
    "url": "https://www.google.com/",
    "search": "https://www.google.com/search?q=${query}"
  },
  "gmaps": {
    "image": "gmaps",
    "description": "Google Maps",
    "url": "https://maps.google.com/maps",
    "search": "https://maps.google.com/maps?q=${query}"
  },
  "gmail": {
    "groups": [
      "mail",
      "social"
    ],
    "image": "gmail",
    "description": "Google Mail",
    "url": "https://mail.google.com/mail/"
  },
  "gsearch": {
    "image": "gsearch",
    "description": "Google Search",
    "url": "https://search.google.com/search-console"
  },
  "gsuite": {
    "image": "gsuite",
    "description": "GSuite",
    "url": "https://admin.google.com/"
  },
  "gtrends": {
    "image": "gtrends",
    "description": "Google Trends",
    "url": "https://trends.google.es/trends/"
  },
  "howjsay": {
    "image": "howjsay",
    "description": "How J Say",
    "url": "http://howjsay.com/",
    "search": "http://howjsay.com/index.php?word=${query}"
  },
  "kickstart": {
    "image": "kickstart",
    "description": "Kickstarter",
    "url": "https://www.kickstarter.com/",
    "search": "https://www.kickstarter.com/discover/advanced?ref=nav_search&term=${query}"
  },
  "linkedin": {
    "groups": [
      "social"
    ],
    "image": "linkedin",
    "description": "Linkedin",
    "url": "https://www.linkedin.com",
    "search": "https://www.linkedin.com/search/results/all/?keywords=${query}"
  },
  "mailerlite": {
    "image": "mailerlite",
    "description": "Mailerlite",
    "url": "https://app.mailerlite.com/"
  },
  "mouser": {
    "groups": [
      "shop"
    ],
    "image": "mouser",
    "description": "Mouser",
    "url": "https://www.mouser.es",
    "search": "https://www.mouser.es/Search/Refine.aspx?Keyword=${query}"
  },
  "netflix": {
    "groups": [
      "multimedia"
    ],
    "image": "netflix",
    "description": "Netflix",
    "url": "https://www.netflix.com/",
    "search": "https://www.netflix.com/search?q=${query}"
  },
  "namecheap": {
    "image": "namecheap",
    "description": "Namecheap",
    "url": "https://www.namecheap.com/",
    "search": "https://www.namecheap.com/domains/registration/results/?domain=${query}"
  },
  "noarsensors": {
    "image": "noarsensors",
    "description": "Noar Sensors",
    "url": "https://noarsensors.com/"
  },
  "openbank": {
    "image": "openbank",
    "description": "Openbank",
    "url": "https://www.openbank.es/"
  },
  "overleaf": {
    "image": "overleaf",
    "description": "Overleaf",
    "url": "https://www.overleaf.com/"
  },
  "oxdict": {
    "groups": [
      "dictionary"
    ],
    "image": "oxdict",
    "description": "Oxford Dictionary",
    "url": "http://www.oxforddictionaries.com/",
    "search": "http://www.oxforddictionaries.com/search/all/?direct=1&q=${query}"
  },
  "pordede": {
    "groups": [
      "multimedia"
    ],
    "image": "pordede",
    "description": "Pordede",
    "url": "http://www.pordede.com/"
  },
  "primevideo": {
    "groups": [
      "multimedia"
    ],
    "image": "primevideo",
    "description": "Prime Video",
    "url": "https://www.primevideo.com/",
    "search": "https://www.primevideo.com/search/ref=atv_nb_sr?phrase=${query}&ie=UTF8"
  },
  "rae": {
    "groups": [
      "dictionary"
    ],
    "image": "rae",
    "description": "Real Academia",
    "url": "http://dle.rae.es/",
    "search": "http://dle.rae.es/?w=${query}"
  },
  "reddit": {
    "groups": [
      "social"
    ],
    "image": "reddit",
    "description": "Reddit",
    "url": "https://www.reddit.com/",
    "search": "https://www.reddit.com/search/?q=${query}"
  },
  "rs": {
    "groups": [
      "shop"
    ],
    "image": "rs",
    "description": "RS Online",
    "url": "https://es.rs-online.com/",
    "search": "https://es.rs-online.com/web/c/?searchTerm=${query}&sra=oss&r=t"
  },
  "spotify": {
    "groups": [
      "multimedia"
    ],
    "image": "spotify",
    "description": "Spotify",
    "url": "https://play.spotify.com/",
    "search": "https://play.spotify.com/search/${query}/"
  },
  "twitter": {
    "groups": [
      "social"
    ],
    "image": "twitter",
    "description": "Twitter",
    "url": "https://twitter.com/",
    "search": "https://twitter.com/search?q=${query}"
  },
  "whatsapp": {
    "image": "whatsapp",
    "description": "Whatsapp Web",
    "url": "https://web.whatsapp.com/"
  },
  "wikien": {
    "image": "wikien",
    "description": "Wikipedia EN",
    "url": "http://en.wikipedia.org/",
    "search": "http://en.wikipedia.org/w/index.php?search=${query}"
  },
  "wikies": {
    "image": "wikies",
    "description": "Wikipedia ES",
    "url": "http://es.wikipedia.org/",
    "search": "http://es.wikipedia.org/w/index.php?search=${query}"
  },
  "wordref": {
    "groups": [
      "translation"
    ],
    "image": "wordref",
    "description": "Word Reference",
    "url": "http://www.wordreference.com/",
    "search": "http://www.wordreference.com/es/translation.asp?tranword=${query}"
  },
  "youtube": {
    "groups": [
      "multimedia"
    ],
    "image": "youtube",
    "description": "YouTube",
    "url": "http://www.youtube.com/",
    "search": "http://www.youtube.com/results?search_query=${query}"
  }
};
