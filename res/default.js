var config = [
  {
    "name": "Búsqueda",
    "links": [
      "google",
      "gimages",
      "gmaps",
      "wikies",
      "wikien",
      "rae"
    ]
  },
  {
    "name": "Social",
    "links": [
      "youtube",
      "linkedin",    
      "facebook",
      "twitter"
    ]
  },
  {
    "name": "Trabajo",
    "links": [
      "gmail",
      "gitlab",
      "colnix",
      "dearquer"
    ]
  }
];
