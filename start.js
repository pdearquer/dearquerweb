
/* Key constants */
var KEY_F1 = 112;
var KEY_F12 = 123;
var KEY_ENTER = 13;
var KEY_UP = 38;
var KEY_DOWN = 40;
var KEY_ESC = 27;


/**
 * The mouse has clicked on a suggestion.
 */
function qs_sug_click(event)
{
   var query = document.getElementById("qs_query");
   query.value = event.target.firstChild.nodeValue;
   qs_update(query.value, false);
}

/**
 * The mouse is over a suggestion.
 */
function qs_sug_over(event)
{
   var sug = document.getElementById("qs_suggest");
   for(var i = 0; i < sug.children.length; i++)
   {
      sug.children[i].className = "";
   }
   
   event.target.className = "qs_sug_sel";
}

/**
 * Move the suggestion selected to the next down or up.
 * @param down true if go down and false if go up.
 */
function qs_sug_move(down)
{
   var sug = document.getElementById("qs_suggest");
   var query = document.getElementById("qs_query");
   
   var index = -1;
   for(var i = 0; i < sug.children.length; i++)
   {
      if(sug.children[i].className != "")
      {
         index = i;
         break;
      }
   }
   
   if(index == -1)
   {
      if(down)
         index = 0;
      else
         index = sug.children.length - 1;
   }
   else
   {
      sug.children[index].className = "";
      
      if(down)
      {
         index++;
         if(index >= sug.children.length)
            index = 0;
      }
      else
      {
         index--;
         if(index < 0)
            index = sug.children.length - 1;
      }
   }
   
   var el = sug.children[index];
   el.className = "qs_sug_sel";
   query.value = el.firstChild.nodeValue;
}

/**
 * Updates the suggestion box.
 */
function qs_suggest(data)
{
	data = data[1];   
		
	var sug = document.getElementById("qs_suggest");
	sug.innerHTML = "";
	var query = document.getElementById("qs_query");
	if(data.length == 0 || query.value.length == 0)
	{
		sug.style.display = "none";
		return;
	}
	
	var query_wrap = document.getElementById("qs_query_wrap");
   var pos = query_wrap.getBoundingClientRect();
   sug.style.left = pos.left + "px";
   sug.style.top = pos.bottom + "px";
   sug.style.width = (pos.right - pos.left - 2) + "px";
   sug.style.display = "block";
   
   for(var i = 0; i < data.length; i++)
   {
      var div = document.createElement("div");
      div.innerHTML = data[i];
   	div.addEventListener("click", qs_sug_click);
   	div.addEventListener("mouseover", qs_sug_over);
   	sug.appendChild(div);
   }
}

/**
 * Update all the links with the given query.
 */
function qs_update(rawquery, suggest)
{
   query = encodeURIComponent(rawquery);
   
   for(var i = 0; i < qs.length; i++)
   {
      if(qs[i].updater != null)
      {
         var a = document.getElementById(qs[i].id);
         a.href = qs[i].updater(query, rawquery);
      }
   }
   
   if(!suggest)
      return;
   
   if(qs_callback_script != null)
   {
	   document.body.removeChild(qs_callback_script);
	   qs_callback_script = null;
	}
	
   if(query.length > 0)
   {
      fetch("https://suggestqueries.google.com/complete/search?client=chrome&q=" + query)
         .then(response => response.json())
         .then(data => qs_suggest(data));
   }
   else
   {
	   var sug = document.getElementById("qs_suggest");
   	sug.style.display = "none";
   }
}

/**
 * Follow a link by its number
 */
function qs_follow(number, newtab)
{
   if(number > qs.length)
      return;
   
   var a = document.getElementById(qs[number].id);
   if(newtab)
      window.open(a.href, "_blanck");
   else
      window.location = a.href;
}

/**
 * Key down event handler.
 * Function keys must be processed here.
 */
function qs_key_down(event)
{
   if(!event)
      event = window.event;
   var k = event.keyCode;
   if(event.charCode && k == 0)
      k = event.charCode;
   var ctrl = event.ctrlKey;
	
   var query = document.getElementById("qs_query");
   var processed = false;
   
   if(k >= KEY_F1 && k <= KEY_F12)
   {
      qs_update(query.value, false);
      qs_follow(k - 112, ctrl);
      processed = true;
   }
   else if(k == KEY_ENTER)
   {
      qs_update(query.value, false);
      qs_follow(0, ctrl);
      processed = true;
   }
   else if(k == KEY_UP)
   {
      qs_sug_move(false);
      qs_update(query.value, false);
      processed = true;
   }
   else if(k == KEY_DOWN)
   {
      qs_sug_move(true);
      qs_update(query.value, false);
      processed = true;
   }
   else
   {
      query.focus();
   }
   
   if(processed)
   {
      try
      {
         event.preventDefault();
      }
      catch(ex)
      {
         event.returnValue = false;
      }
      return false;
   }
   else
      return true;
}

/**
 * Resets the query box.
 */
function qs_reset()
{
   var query = document.getElementById("qs_query");
   query.value = "";
   qs_update(query.value, true);
   query.focus();
}

/**
 * Key up event handler.
 * ESC must be processed here.
 */
function qs_key_up(event)
{
   if(!event)
      event = window.event;
   var k = event.keyCode;
   if(event.charCode && k == 0)
      k = event.charCode;
   
   var processed = false;
   
   if(k == KEY_ESC)
   {
      qs_reset();
      processed = true;
   }
   /* FIX: Update moved to qs_change function (oninput).
   else if(k != KEY_UP && k != KEY_DOWN && k != KEY_ENTER && !(k >= KEY_F1 && k <= KEY_F12))
   {
      qs_update(query.value, true);
   }
   */
      
   if(processed)
   {
      try
      {
         event.preventDefault();
      }
      catch(ex)
      {
         event.returnValue = false;
      }
      return false;
   }
   else
      return true;
}

/**
 * Mouse click on the document.
 */
function qs_click(event)
{
   el = event.target;
   var query = document.getElementById("qs_query");
   if(query == el || query.contains(el))
      return;
   
   var sug = document.getElementById("qs_suggest");
   /* FIX: Clicking a suggestion now hides the list.   
   if(sug == el || sug.contains(el))
      return;
   */
   sug.style.display = "none";
}

/**
 * Query input change notification (includes copy & paste operations).
 * Uses oninput event.
 */
function qs_change()
{
   var query = document.getElementById("qs_query");
   qs_update(query.value, true);
}

/**
 * Build the searching bar.
 */
function qs_init()
{
   document.getElementById("qs_search").innerHTML =
         '<div id="qs_query_wrap"><img id="qs_reset" src="icons/reset_icon.png" />' +
         '<input id="qs_query" type="text" autocomplete=\"off\" autofocus/></div>' +
         '<div id="qs_suggest"></div>';
         
   document.getElementById("qs_query").addEventListener("input", qs_change);
   document.getElementById("qs_query").focus();
   
   if(window.addEventListener)
   {
      window.addEventListener("keydown", qs_key_down, true);
      window.addEventListener("keyup", qs_key_up, true);
   }
   else if(document.attachEvent)
   {
      document.attachEvent("onkeydown", qs_key_down);
      document.attachEvent("onkeyup", qs_key_up);
   }
   else
   {
      document.onkeydown = qs_key_down;
      document.onkeyup = qs_key_up;      
   }
   document.onclick = qs_click;

   var reset = document.getElementById("qs_reset");
   reset.onclick = qs_reset;

   if("onhelp" in window)
   {
      window.onhelp = function () { return false; };
   }
}

/**
 * Attaches the events of a link.
 * The updater function can be null.
 */
function qs_attach_link(id, updater)
{
   var link = new Object();
   link.id = id;
   link.updater = updater;
   
   qs.push(link);
   var index = qs.length;
   
   var a = document.getElementById(id);
   var els = a.getElementsByTagName("*");
   for(var i = 0; i < els.length; i++)
   {
      if(els[i].className == "qs_info")
      {
         if(index == 1)
            els[i].innerHTML = "Tecla F" + index + " o Intro";
         else if(index <= 12)
            els[i].innerHTML = "Tecla F" + index;
         else
            els[i].innerHTML = "Sin tecla";
         break;
      }
   }
   
   if(updater != null)
   {
      var query = document.getElementById("qs_query").value;
      query = encodeURIComponent(query);      
      a.href = updater(query);
   }
}

/**
 * Appends a new category to the list.
 */
function qs_add_category(name)
{
}

/**
 * Appends a new link to the list.
 */
function qs_add_link(id, image, desc, url, searchUrl)
{
   document.getElementById("qs_links").innerHTML +=
         '<a id="qsl_' + id + '" class="qs_link" title="' + desc + '" href="' + url + '">' + 
         '<div class="qs_desc"><h2>' + desc + '</h2><span class="qs_info">Inicio</span></div>' +
         '<img src="img/' + image + '.png" alt="' + desc + '" /></a>';

   if(searchUrl)
   {
      qs_attach_link("qsl_" + id, function (query, rawquery)
      {
         if(query == "")
            return url;
         else
            return searchUrl.replace("${query}", query);
      });
   }
   else
   {
      qs_attach_link("qsl_" + id);
   }
}

/**
 * List initialization.
 */
var qs = new Array();

/**
 * Callback script element.
 */
var qs_callback_script = null;

/* Init on load */
document.addEventListener("DOMContentLoaded", function()
{
   qs_init();

   config.forEach(category =>
   {
      qs_add_category(category.name)
      for(var i = 0; i < category.links.length; i++)
      {
         var link = category.links[i];
         if(typeof link === 'string')
         {
            id = link;
            link = database[id];
         }
         else
         {
            id = link.image + "_" + i;
         }
         qs_add_link(id, link.image, link.description, link.url, link.search);
      }
   })
});

